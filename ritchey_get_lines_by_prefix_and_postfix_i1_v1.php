<?php
#Name:Ritchey Get Lines By Prefix And Postfix i1 v1
#Description:Get the contents of lines in a file between line containing a prefix, and line containing a postfix. Returns lines on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is the file to read from. 'prefix' (required) is the prefix line to search for. 'postfix' (required) is the postfix line to search for. 'retain_prefix_and_postfix' (optional) specifies whether to retain the prefix string and postfix string in the return. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,prefix:string:required,postfix:string:required,retain_prefix_and_postfix:bool:optional,display_errors:bool:optional
#Content:
if (function_exists('ritchey_get_lines_by_prefix_and_postfix_i1_v1') === FALSE){
function ritchey_get_lines_by_prefix_and_postfix_i1_v1($source, $prefix, $postfix, $retain_prefix_and_postfix = NULL, $display_errors = NULL){
	$errors = array();
	if (@is_file($source) === FALSE){
		$errors[] = "source";
	}
	if (@isset($prefix) === TRUE){
		if ($prefix === ''){
			$errors[] = "prefix";
		}
	} else {
		$errors[] = "prefix";
	}
	if (@isset($postfix) === TRUE){
		if ($postfix === ''){
			$errors[] = "postfix";
		}
	} else {
		$errors[] = "postfix";
	}
	if ($retain_prefix_and_postfix === NULL){
		$retain_prefix_and_postfix = TRUE;
	} else if ($retain_prefix_and_postfix === TRUE){
		#Do Nothing
	} else if ($retain_prefix_and_postfix === FALSE){
		#Do Nothing
	} else {
		$errors[] = "retain_prefix_and_postfix";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		$handle = @fopen($source, 'r');
		$line = '';
		$capture = array();
		$switch1 = FALSE;
		$check_prefix = FALSE;
		$check_postfix = FALSE;
		while (@feof($handle) !== TRUE AND $switch1 !== TRUE) {
			$check_pass_completed = FALSE;
			$line = @fgets($handle);
			###Capture prefix
			if ($check_pass_completed === FALSE){
				if (@rtrim($line) === $prefix){
					if ($check_prefix === FALSE){
						$capture[] = $line;
						$check_prefix = TRUE;
						$check_pass_completed = TRUE;
					}
				}
			}
			###If prefix captured, capture postfix
			if ($check_pass_completed === FALSE){
				if ($check_prefix === TRUE){
					if (@rtrim($line) === $postfix){
						$capture[] = $line;
						$check_postfix = TRUE;
						$check_pass_completed = TRUE;
						$switch1 = TRUE;
					}
				}
			}
			###If prefix captured, capture following lines, so long as not postfix
			if ($check_pass_completed === FALSE){
				if ($check_prefix === TRUE){
					if ($check_postfix === FALSE){
						$capture[] = $line;
						$check_pass_completed = FALSE;
					}
				}
			}
		}
		@fclose($handle);
		###Delete captured lines if both prefix and postfix weren't present
		if ($check_prefix === FALSE){
			$capture = NULL;
			$errors[] = 'capture';
		} else if ($check_postfix === FALSE){
			$capture = NULL;
			$errors[] = 'capture (2)';
		}
		###Determine if the capture is empty
		if (@count($capture) < 3){
			$errors[] = "capture (3)";
		}
		###Determine if the prefix_and_postfix should be retained.
		if ($retain_prefix_and_postfix === FALSE){
			@array_shift($capture);
			@array_pop($capture);
		}
		###Convert capture to string
		$capture = @implode('', $capture);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_get_lines_by_prefix_and_postfix_i1_v1_format_error') === FALSE){
				function ritchey_get_lines_by_prefix_and_postfix_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_get_lines_by_prefix_and_postfix_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $capture;
	} else {
		return FALSE;
	}
}
}
?>